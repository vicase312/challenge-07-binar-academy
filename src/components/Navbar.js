// import { useSelector } from "react-redux"
import { Link, useLocation } from "react-router-dom"
import { useState, useEffect } from "react"

// import { Link } from "react-router-dom"

const Navbar = () => {
    // const title = useSelector(state => state.ui.title)    
    const location = useLocation()
    const [showButton, setShowButton] = useState(false)
    useEffect(() => {
        setShowButton(location.pathname === "/")
    }, [location.pathname])
    return (
            <section className="hero py-5">
        <div className="container">
            <div className="container  fixed-top">

                <nav className="navbar navbar-expand-lg navbar-light bg-transparent p-0 pt-2">
                    <Link className="navbar-brand" to="/">&nbsp;</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                        <ul className="navbar-nav">
                            <li className="d-flex justify-content-between d-lg-none">
                                <span><b>BCR</b></span><a className="navbar-nav-toggler" href="/#">X</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/#our">Our Services</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/#why">Why Us</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/#test">Testimonial</a>
                            </li>
                            <li className="nav-item mb-2">
                                <a className="nav-link" href="/#faq">FAQ</a>
                            </li>
                            <li className="nav-item">
                                <button className="btn btn-success font-weight-bolder">Register</button>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div className="hero-row row pt-5">
                <div className="col-lg-5 col-md-12 my-auto">
                    <h2 className="font-weight-bolder">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h2>
                    <p className="text-justify pr-4">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
                        terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24
                            jam.</p>
                        {showButton ?
                        <Link to="/cars"><button className="btn btn-success font-weight-bolder">Mulai Sewa Mobil</button></Link>
                            :
                            ""
                    }
                </div>
                <div className="col-lg-7 col-md-12">
                    <img className="mobil img-fluid" src="/assets/img/mobil.svg" alt="GambarMobil"/>
                </div>
            </div>
        </div>
    </section>
    )
}

export default Navbar
