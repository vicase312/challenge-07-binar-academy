const Footer = () => {
    return (
        <footer className="footer">

        <div className="container">
            <div className="row">

                <div className="col-lg-3 col-md-12">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div className="col-lg-3 col-md-12">
                    <p className="font-weight-bold">Our services</p>
                    <p className="font-weight-bold">Why Us</p>
                    <p className="font-weight-bold">Testimonial</p>
                    <p className="font-weight-bold">FAQ</p>
                </div>
                <div className="col-lg-3 col-md-12">
                    <p>Connect with us</p>
                    <div className="">
                        <img src="/assets/img/icon/facebook.svg" alt="fb"/>
                        <img src="/assets/img/icon/instagram.svg" alt="ig"/>
                        <img src="/assets/img/icon/twitter.svg" alt="tw"/>
                        <img src="/assets/img/icon/mail.svg" alt="mail"/>
                        <img src="/assets/img/icon/twitch.svg" alt="tct"/>
                    </div>
                </div>
                <div className="col-lg-3 col-md-12">
                    <p>Copyright Binar 2022</p>
                    <img src="/assets/img/logo.svg" alt="logo"/>
                </div>
            </div>

        </div>
    </footer>
    )
}

export default Footer